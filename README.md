# JastAdd Type Inference Demo 2

This implementation of type inference relies on type placeholders
(`InferredType`) to infer the expected type of declarations (variables and
parameters).

To illustrate type inference a very minimal language is used. The language
mostly consists of function and variable declarations, lambdas, and literals
(boolean and integer). Variable declarations may be typed or untyped.
A typed variable declaration looks like this:

```
v : int = 4;
```

An untyped variable declaration looks like this:

```
v := 4;
```

The type of an untyped variable declaration depends on its initializer
expression.  Lambda parameter types are inferred based on the context where the
lambda is used, for example:

```
l : int->int = x -> 4;
```

If the function type for the lambda can not be inferred then
the type of the lambda parameter is unknown.

Inferred types are not always well-defined. The following gives an error:

```
x := z -> true;
```

The type of `x` can not be inferred because it depends on the type of `z` which
in turn depends on the type of `x`.
