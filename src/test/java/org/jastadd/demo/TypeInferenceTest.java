package org.jastadd.demo;

import org.junit.Test;

import java.util.Collection;
import java.util.ArrayList;

import static com.google.common.truth.Truth.assertThat;

/**
 * Author: Jesper Öqvist
 */
public class TypeInferenceTest {

  @Test public void testSimple01() {
    /*
     * This program has no type errors:
     *
     * void f(x : int, b : bool) {
     * }
     *
     * void g() {
     *   f(1, false);
     * }
     *
     */
    Program p = program(
        fun("f", list(param("x", intType()), param("b", boolType())), list()),
        fun("g", list(), list(
            exprStmt(call("f", intLit(1), falseLit()))
          )));
    assertThat(p.typeErrors()).isEmpty();
  }

  @Test public void testInference01() {
    /*
     * Untyped variable declaration:
     *
     * void f() {
     *   v := true;
     * }
     *
     */
    Program p = program(
        fun("f", list(), list(
            varDecl("v", trueLit())
          )));
    assertThat(p.typeErrors()).isEmpty();
  }

  @Test public void testInference02() {
    /*
     * This program has no type errors:
     *
     * void f(fun : int -> int) {
     * }
     *
     * void g() {
     *   f(x -> 8);
     * }
     *
     */
    Program p = program(
        fun("f", list(param("fun", funType(list(intType()), intType()))), list()),
        fun("g", list(), list(
            exprStmt(call("f", closure(list(param("x")), intLit(8))))
          )));
    assertThat(p.typeErrors()).isEmpty();
  }

  @Test public void testFail01() {
    /*
     * The types of v and x can not be inferred:
     *
     * void f() {
     *   v := x -> 9;
     * }
     *
     */
    Program p = program(
        fun("f", list(), list(
            varDecl("v", closure(list(param("x")), intLit(9)))
          )));
    assertThat(p.typeErrors()).containsExactly(
        "Can not infer type of variable v",
        "Can not infer type of parameter x");
  }


  // Helper methods to construct ASTs:

  private static <T extends ASTNode> List<T> list(T... args) {
    if (args == null) {
      return new List<T>();
    }
    List<T> list = new List<T>();
    for (T item : args) {
      list.add(item);
    }
    return list;
  }

  private static Program program(FunDecl... fun) {
    return new Program(list(fun));
  }

  private static FunDecl fun(String name, List<Param> params, List<Stmt> stmts) {
    return new FunDecl(voidType(), name, params, stmts);
  }

  private static Param param(String name, Type type) {
    return new Param(type, name);
  }

  // Untyped parameter declaration - used in closures.
  private static Param param(String name) {
    return new Param(new InferredType(), name);
  }

  private static FunType funType(List<Type> params, Type result) {
    return new FunType(params, result);
  }

  private static Call call(String name, Expr... args) {
    return new Call(name, list(args));
  }

  private static Closure closure(List<Param> params, Expr expr) {
    return new Closure(params, expr);
  }

  private static VoidType voidType() {
    return new VoidType();
  }

  private static IntType intType() {
    return new IntType();
  }

  private static BoolType boolType() {
    return new BoolType();
  }

  private static IntLiteral intLit(int value) {
    return new IntLiteral("" + value);
  }

  private static True trueLit() {
    return new True();
  }

  private static False falseLit() {
    return new False();
  }

  private static ExprStmt exprStmt(Expr expr) {
    return new ExprStmt(expr);
  }

  private static VarDecl varDecl(String name, Expr expr) {
    return new VarDecl(new InferredType(), name, expr);
  }
}
